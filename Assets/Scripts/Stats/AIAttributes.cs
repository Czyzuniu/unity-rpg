﻿
public class AIAttributes : Attributes
{
    public override void OnDeath() {
        gameObject.AddComponent<RagdollDeathHumanoid>();
        GameManager.instance.OnCombatEnd(gameObject);
    }
}
