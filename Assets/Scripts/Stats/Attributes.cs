﻿using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

public class Attributes : MonoBehaviour
{
    public Entity entity;
    [SerializeField]
    public SerializableDictionaryBase<Attribute, float> attributes;
    public RuntimeAnimatorController defaultAnimatorController;
    public StateManager stateManager;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
       loadStartingAttributes();
    }

    public virtual bool OnDamageTaken() {
        return true;
    }

    public void loadStartingAttributes() {
        defaultAnimatorController = GetComponent<Animator>() ? GetComponent<Animator>().runtimeAnimatorController : null;
        stateManager = GetComponent<StateManager>();
        attributes[Attribute.Health] = entity.level * 100;
        attributes[Attribute.Mana] = entity.level * 100;
    }

    public float GetAttributeValue(Attribute attribute) {
        if (attributes.ContainsKey(attribute)) {
            return attributes[attribute];
        }
        return 0;
    }

    public void ApplyValueToAttribute(Attribute attribute, float modifier) {
        if (attributes.ContainsKey(attribute)) {
            float currentValue = attributes[attribute];
            currentValue += modifier;
            attributes[attribute] = currentValue;
            OnAttributeChanged();
        }
    }

    public virtual void OnDeath() {
        gameObject.AddComponent<RagdollDeath>();
        AudioManager.instance.TogglePlay("DeathMusic");
    }

    void DeathCheck() {
        if (GetAttributeValue(Attribute.Health) <= 0) {
            stateManager.isDead = true;
            OnDeath();
        }
    }

    void OnAttributeChanged() {
        DeathCheck();
    }

    
}
