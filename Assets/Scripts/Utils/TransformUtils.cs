using UnityEngine;

public static class TransformUtils {
    static public Transform GetChildGameObject(GameObject fromGameObject, string withName) {
         Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(false);
         foreach (Transform t in ts) if (t.gameObject.name == withName) return t;
         return null;
     }

     static public void SetAnimator(Transform target, RuntimeAnimatorController controller) {
        Animator currentAnimator = target.GetComponent<Animator>();
        if (currentAnimator) {
            currentAnimator.runtimeAnimatorController = controller;
        }
     }
}