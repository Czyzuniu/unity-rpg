﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AIBase : MonoBehaviour
{
    public Transform target;
    protected NavMeshAgent agent;
    protected AIAttributes attributes;
    protected Animator animator;
    protected StateManagerAI stateManager;
    protected AttackControllerAI attackController;
    protected WeaponController weaponController;
    private Vector3 startPos;
    private NonPlayableEntity NPC;
    private float nextTimeToAttack;
    public float proximityRange;
    public float proximityDecideRange;

    [SerializeField]
    private int combatDirection;

    public delegate void OnCombatStarted();
    public static OnCombatStarted CombatStarted;

    public delegate void OnCombatEnded();
    public static OnCombatEnded CombatEnded;

    private bool shouldWalkAway;

    void Start() {
        attributes = GetComponent<AIAttributes>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        stateManager = GetComponent<StateManagerAI>();
        attackController = GetComponent<AttackControllerAI>();
        weaponController = GetComponent<WeaponController>();
        startPos = transform.position;
        NPC = (NonPlayableEntity) attributes.entity;
        agent.speed = NPC.walkSpeed;
        combatDirection = Direction();
    }

    // Update is called once per frame
    void Update()
    {
        Behaviour();
    }

    int Direction() {
        return Random.Range(-1f, 1f) > 0 ? 1: -1;
    }

    public void SetTarget(Transform target) {
        this.target = target;
        if (target) {
            StartCoroutine(ShouldAttack());
            stateManager.state = AIBehaviourState.CHASE;
        } else {
            stateManager.state = AIBehaviourState.IDLE;
        }
    }

    public void ClearTarget() {
        target = null;
    }

    public virtual void InAttackRange() {
        InAttackRange(AIBehaviourState.IN_PROXIMITY);
    }

    public virtual void InAttackRange(AIBehaviourState previousState) {
        stateManager.InAttackRange = InRange(agent.stoppingDistance, AIBehaviourState.ATTACK, previousState);
    }

    public bool InDistance(float limit) {
        return Vector3.Distance(transform.position, target.transform.position) <= limit;
    }

    public bool InRange(float limit, AIBehaviourState successState, AIBehaviourState failState) {
        bool inRange = InDistance(limit);
        if (inRange) {
            stateManager.state = successState;
        } else {
            stateManager.state = failState;
        }

        return inRange;
    }

    public virtual void InCombatRange() {
        stateManager.InCombatRange = InRange(proximityRange,AIBehaviourState.IN_PROXIMITY, AIBehaviourState.CHASE);
    }

    public virtual bool CanMove() {
        return stateManager.isHit == false;
    }

    public virtual void CheckForAttack() {
        int layerMask = 1 << 9;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, NPC.aggroDistance, layerMask);
        if (hitColliders.Length > 0) {
            GameObject potentialTarget = hitColliders[0].gameObject;
            StateManager targetState = potentialTarget.GetComponent<StateManager>();
            if (!targetState.isDead) {
                SetTarget(potentialTarget.transform);
                GameManager.instance.OnCombatStart(gameObject);
            }
        }
    }

    public virtual void StartCombat() {
        if (!stateManager.isInCombatStance) {
            weaponController.StartCombatMode();
        }
    }

    public virtual void EndCombat() {
        if (stateManager.isInCombatStance) {
            weaponController.EndCombatMode();
        }
        agent.speed = NPC.walkSpeed;
        agent.SetDestination(startPos);
        ClearTarget();
    }

    public void Behaviour() {
        // float currentSpeed = agent.velocity.magnitude/agent.speed;

        switch (stateManager.state) {
          case AIBehaviourState.IDLE:
              Idle();
              break;
          case AIBehaviourState.HIT:
              OnHitAction();
              break;
          case AIBehaviourState.PATROL:
              Idle();
              break;
          case AIBehaviourState.CHASE:
              Chase();
              InCombatRange();
              break;
          case AIBehaviourState.IN_PROXIMITY:
              InProximity();
              break;
          case AIBehaviourState.IN_PROXIMITY_DECIDING:
              InProximityDeciding();
              break;
          case AIBehaviourState.IN_PROXIMITY_TRY_ATTACK:
              TryAttack();
              break;
          case AIBehaviourState.ATTACK:
              Attack();
              break;
      }

        Vector3 normalizedMovement = agent.desiredVelocity.normalized;

        Vector3 forwardVector = Vector3.Project(normalizedMovement, transform.forward);

        Vector3 rightVector = Vector3.Project (normalizedMovement, transform.right);

        // Dot(direction1, direction2) = 1 if they are in the same direction, -1 if they are opposite

        float forwardVelocity = forwardVector.magnitude * Vector3.Dot(forwardVector, transform.forward);

        float rightVelocity = rightVector.magnitude * Vector3.Dot(rightVector, transform.right);

        
        agent.isStopped = stateManager.IsAttacking;

        if (!agent.isStopped) {
            animator.SetFloat("Speed", forwardVelocity);
            animator.SetFloat("Direction", rightVelocity);
        }

        animator.SetLayerWeight(animator.GetLayerIndex("HitWalk"), Mathf.Clamp(Mathf.Abs(animator.GetFloat("Speed")) + Mathf.Abs(animator.GetFloat("Direction")) , 0, 1));

        if (target) {
           transform.LookAt (new Vector3 (target.position.x, transform.position.y, target.position.z));
        }
        
        animator.SetBool("IsChasing", stateManager.state.Equals(AIBehaviourState.CHASE));

    }

    public Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        Vector3 randDirection =  Random.insideUnitSphere * dist + target.position;
        randDirection += origin;
        NavMeshHit navHit;
        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);
        return navHit.position;
    }

    public virtual void Patrol() {
        agent.speed = NPC.walkSpeed;
    }

    public virtual void Idle() {
        agent.speed = 0;
        animator.SetFloat("Speed", 0);
        animator.SetFloat("Direction", 0);
        CheckForAttack();
        nextTimeToAttack = Time.time + 2.5f;
    }

    public virtual void Move() {
        
        agent.ResetPath();

        animator.SetBool("InCombatStance", stateManager.isInCombatStance);
        animator.SetBool("InAttackRange", stateManager.InAttackRange);

        agent.SetDestination(target.position);
    }

    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
        return Quaternion.Euler(angles) * (point - pivot) + pivot;
    }

    public virtual void WalkInDirection(Vector3 direction, float speed) {
        WalkInDirection(direction, speed, Vector3.zero);
    }

    public virtual void WalkInDirection(Vector3 direction, float speed, Vector3 toward) {
        agent.ResetPath();
        Vector3 targetPosition = direction * speed + toward;
        agent.destination = transform.position + targetPosition;
    }

    public virtual void InProximityDeciding() {
        WalkInDirection(transform.right * combatDirection, agent.speed);
    }

    public virtual void Parry(bool isBlocking) {
        animator.SetBool("IsBlocking", isBlocking);
        if (isBlocking) {
            agent.ResetPath();
            StartCoroutine(StopParry(2.5f));
        }
    }

     IEnumerator ShouldAttack() {
        while(true) { 
            if (stateManager.state.Equals(AIBehaviourState.IN_PROXIMITY_DECIDING)) {
                yield return new WaitForSeconds(InDistance(agent.stoppingDistance) ? 0.1f : Random.Range(1.5f, 3.5f));
                stateManager.state = AIBehaviourState.IN_PROXIMITY_TRY_ATTACK;
            } else {
                yield return null;
            }
        }
    }

    IEnumerator StopParry(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        Parry(false);
    }

    public virtual void TryAttack() {
        WalkInDirection(transform.forward, agent.speed);
        InAttackRange(AIBehaviourState.IN_PROXIMITY_TRY_ATTACK);
    }

    public virtual void InProximity() {
        agent.speed = NPC.combatWalkSpeed;
        if (InRange(proximityDecideRange, AIBehaviourState.IN_PROXIMITY_DECIDING, AIBehaviourState.IN_PROXIMITY)) {
            combatDirection = Direction();
            StopCoroutine("ResetDirection");
            StartCoroutine("ResetDirection");
        } else {
            InCombatRange();
            Move();
        }
        
    }

    IEnumerator ResetDirection() 
    {
       float duration = Random.Range(1.5f, 3.5f); 
       yield return new WaitForSeconds(duration);
    }

    public virtual void Chase() {
        agent.speed = NPC.chaseSpeed;
        StartCombat();
        Move();
    }

    public virtual void Hit(int toggle) {
        agent.ResetPath();
        animator.ResetTrigger("Attack");
        if (toggle == 1) {
            stateManager.state = AIBehaviourState.HIT; 
            shouldWalkAway = Random.Range(0, 100) > 50 ? true : false;
            if (!shouldWalkAway) {
                Parry(true);
            }
        } else {
            stateManager.state = AIBehaviourState.CHASE; 
        }
    }

    public virtual void OnHitAction() {
        if (shouldWalkAway) {
            WalkInDirection(-transform.forward, agent.speed);
        }
    }

    public virtual void Attack() {
        animator.SetBool("IsBlocking", false);
        InAttackRange();
        attackController.Attack();
    }

    public virtual void OnSuccessBlock() {
        StateManager targetState = target.GetComponent<StateManager>();
        if (targetState.canAttack) {
            Parry(true);
        } else {
            stateManager.state = AIBehaviourState.IN_PROXIMITY_TRY_ATTACK;
        }
    }

    public void OnEnable() {
        HitController.BlockSuccess += OnSuccessBlock;
    }
}
