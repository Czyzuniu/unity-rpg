﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour {
    PlayerInputActions inputActions;
    public float gravity = -9.81f;
    public float forwardSpeed = 3f;
    public float combatForwardSpeed = 2.5f;
    public float jumpForce = 1.5f;
    public Transform groundCheckObject;
    public float minGroundDistance = 1f;
    public float turnSpeed = 10f;
    private Animator animator;
    [SerializeField]
    private Vector2 input;
    private Quaternion freeRotation;
    private Camera mainCamera;
    private Vector3 moveDirection;
    private float velocityY;
    public float currentSpeed;
    private CharacterController controller;
    public bool isGrounded;
    [SerializeField]
    private float fallVelocity;

    [SerializeField]
    private Vector3 jumpDirection;

    [SerializeField]
    private Vector3 worldSpaceInput;

    private Vector3 lastDirection;

    private AttackController attackController;
    private Attributes attributes;
    private StateManager stateManager;


    void Awake() {
        inputActions = new PlayerInputActions();
        inputActions.PlayerControls.Jump.performed += ctx => PerformJump();
        inputActions.PlayerControls.Sprint.performed += ctx => Sprint(ctx);
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        mainCamera = Camera.main;
        attackController = GetComponent<AttackController>();
        attributes = GetComponent<Attributes>();
        stateManager = GetComponent<StateManager>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Start() {
        stateManager.CanMove = true;
    }

    void PerformJump() {
        if (!stateManager.inAir) {
            animator.SetTrigger("Jump");
        }
    }

    void Sprint(InputAction.CallbackContext context) {

        //if (context.performed)
        //    animator.SetBool("IsSprinting", true);
        //else if (context.canceled)
        //    animator.SetBool("IsSprinting", false);
    }

    void Jump() {
        if (isGrounded) {
            velocityY = Mathf.Sqrt(jumpForce * -2f * gravity);
            stateManager.IsJumping = true;
            lastDirection = worldSpaceInput.normalized;
        }
    }

    void JumpStart() {
        Jump();
    }

    public void JumpEnd() {
        stateManager.IsJumping = false;
        //float angle = Vector3.Angle(lastDirection, worldSpaceInput.normalized);
        //print(angle);
        //if (angle > 75) {
        //    currentSpeed = 0;
        //}
    }

    //public bool OnSlope() {
    //    int layerMask = ~LayerMask.GetMask("Player");

    //    if (stateManager.inAir)
    //        return false;

    //    RaycastHit hit;
    //    if (Physics.Raycast(transform.position, Vector3.down, out hit, controller.height / 2 * slopeForceRayLength)) {
    //        return hit.normal != Vector3.up;
    //    }

    //    return false;
    //}

    public bool IsGrounded() {
        int layerMask = ~LayerMask.GetMask("Player");
        RaycastHit hit;
        if (Physics.Raycast(groundCheckObject.position, Vector3.down, out hit, minGroundDistance, layerMask)) {
            return true;
        }
        return false;
    }

    void OnDrawGizmos() {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.1f);
    }

    // Update is called once per frame
    void Update() {
        Vector3 forward = mainCamera.transform.forward;
        Vector3 right = mainCamera.transform.right;
        Vector3 lookDirection = input.x * right + input.y * forward;
        stateManager.IsFalling = velocityY <= fallVelocity;
        stateManager.IsMoving = Mathf.Abs(input.x) > 0 || Mathf.Abs(input.y) > 0;
        animator.SetBool("IsMoving", stateManager.IsMoving);
        animator.SetBool("IsJumping", stateManager.IsJumping);

        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");

        if (stateManager.inAir) {
            lookDirection = CameraRelativeFlatten(Vector3.ClampMagnitude(new Vector3(input.x, 0f, input.y), 1), Vector3.up);
        }

        bool inCombat = stateManager.isInCombatStance;
        bool isStaggered = stateManager.IsStaggered;


        animator.SetFloat("Speed",input.y);
        animator.SetFloat("Direction",input.x);

        animator.SetLayerWeight(animator.GetLayerIndex("HitWalk"), Mathf.Clamp(Mathf.Abs(animator.GetFloat("Speed")) + Mathf.Abs(animator.GetFloat("Direction")) , 0, 1));
        animator.SetLayerWeight(animator.GetLayerIndex("CombatWalk"), Mathf.Clamp(Mathf.Abs(animator.GetFloat("Speed")) + Mathf.Abs(animator.GetFloat("Direction")) , 0, 1));
        //animator.SetLayerWeight(animator.GetLayerIndex("Combat"), -animator.GetLayerWeight(animator.GetLayerIndex("CombatWalk")));

        if (inCombat || input.y < 0) {
            lookDirection = forward;
        }
        if (input != Vector2.zero && input.magnitude > 0.1f  ) {
            freeRotation = Quaternion.LookRotation(lookDirection, transform.up);
            float diferenceRotation = freeRotation.eulerAngles.y - transform.eulerAngles.y;
            float eulerY = transform.eulerAngles.y;
            if (diferenceRotation < 0 || diferenceRotation > 0) {
                eulerY = freeRotation.eulerAngles.y;
            }
            Quaternion targetRotation = Quaternion.Euler(new Vector3(0, eulerY, 0));
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);

        }

        worldSpaceInput = CameraRelativeFlatten(Vector3.ClampMagnitude(new Vector3(input.x, 0f, input.y), 1), Vector3.up) * GetSpeedByBehaviour();
        isGrounded = controller.isGrounded;

        if (!isGrounded && !stateManager.isInWater) {
            velocityY += gravity * Time.deltaTime;
            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Raycast(groundCheckObject.position, -Vector3.up, out hitInfo)) {
                animator.SetFloat("DistanceToGround", hitInfo.distance);
            }
        }


        //if (OnSlope()) {
        //    controller.Move(Vector3.down * controller.height / 2 * slopeForce * Time.deltaTime);
        //}

        if (isGrounded && velocityY < 0) {
            if (velocityY <= -20) {
                animator.SetTrigger("HeavyLanding");
                OnFallLandingStart(velocityY);
            }
            velocityY = -2.5f;
        }


        stateManager.InAir = !isGrounded && !stateManager.isInWater;
        animator.SetBool("InAir", stateManager.InAir);

        worldSpaceInput.y = velocityY;

        animator.SetFloat("VelocityY", velocityY);
        controller.Move(worldSpaceInput * Time.deltaTime);
    }

    Vector3 CameraRelativeFlatten(Vector3 input, Vector3 localUp) {
        // If this script is on your camera object, you can use this.transform instead.

        Transform cam = Camera.main.transform;

        // The first part creates a rotation looking into the ground, with
        // "up" matching the camera's look direction as closely as it can. 
        // The second part rotates this 90 degrees, so "forward" input matches 
        // the camera's look direction as closely as it can in the horizontal plane.
        Quaternion flatten = Quaternion.LookRotation(
                                            -localUp,
                                            cam.forward
                                       )
                                        * Quaternion.Euler(Vector3.right * -90f);

        // Now we rotate our input vector into this frame of reference
        return flatten * input;
    }


    void OnFallLandingStart(float hitForce) {
        float damageMultiplier = 0f;
        stateManager.CanMove = false;
        if (hitForce <= -25) {
            damageMultiplier = 5.5f;
        } else if (hitForce <= -20) {
            damageMultiplier = 2.5f;
        } else if (hitForce <= -15) {
            damageMultiplier = 1.5f;
        }
        float force = hitForce * damageMultiplier;
        float damageToDeal = attributes.GetAttributeValue(Attribute.Health) / 100 * force;
        attributes.ApplyValueToAttribute(Attribute.Health, damageToDeal);
    }

    void OnFallLandingEnd() {
        stateManager.CanMove = true;
    }


    private float GetSpeedByBehaviour() {
        bool inCombat = stateManager.isInCombatStance;

        if (!stateManager.CanMove) {
            return 0;
        }

        if (stateManager.IsMoving) {
            if (inCombat) {
                return combatForwardSpeed;
            }

            if (input.y < 0) {
                return forwardSpeed / 2;
            }

            return forwardSpeed;
        }

        return 0;
    }

    public void OnWaterEnter() {
        print("entered");
        stateManager.isInWater = true;
        velocityY = 0;
        animator.SetBool("InWater", stateManager.isInWater);
    }


    private void OnEnable() {
        HitController.WaterEntered += OnWaterEnter;
        inputActions.Enable();
    }

    private void OnDisable() {
        inputActions.Disable();
    }

    public void Roll() {
        animator.SetTrigger("Roll");
    }

}
