﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RagdollDeathHumanoid : RagdollDeath {

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    void Start() {
        Die();
    }

    void Die() {
        GetComponent<Animator>().enabled = false;
        Destroy(GetComponent<AIBase>());
        Destroy(GetComponent<NavMeshAgent>());
        Destroy(GetComponent<BoxCollider>());
        Destroy(GetComponent<EnemyUI>());
    }
}
