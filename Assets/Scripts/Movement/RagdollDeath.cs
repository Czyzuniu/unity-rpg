﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollDeath : MonoBehaviour {
    protected Animator animator;
    private Cinemachine.CinemachineFreeLook c_VirtualCamera;
    public Transform cameraTarget;

    private void Awake() {
        animator = GetComponent<Animator>();
        cameraTarget = transform.Find("Character/Armature/Hips");
    }

    void Start() {
        c_VirtualCamera = GameManager.instance.mainCamera;
        Die();
    }

    void Die() {
        GetComponent<Animator>().enabled = false;
        Destroy(GetComponent<PlayerMovement>());
        c_VirtualCamera.m_LookAt = cameraTarget;
        c_VirtualCamera.m_Follow = cameraTarget;
    }
}
