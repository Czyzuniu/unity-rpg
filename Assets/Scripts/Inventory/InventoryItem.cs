﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    private Item item;
    public TMP_Text itemName;
    public TMP_Text itemDetails;
    public Image itemImage;
    public Color equippedColor;

    public bool IsEquipped { get;
        set; }


    // Start is called before the first frame update
    void Start()
    {
        item = GetComponent<ItemManager>().entity;
        itemName = transform.Find("ItemName").GetComponent<TMP_Text>();
        itemImage = transform.Find("ItemSprite").GetComponent<Image>();
        itemDetails = transform.Find("ItemDetails").GetComponent<TMP_Text>();
        itemName.text = item.itemName;
        itemDetails.text = item.GetItemDetails();
        itemImage.sprite = item.inventorySprite;
    }

    public Item GetItem() {
        return item;
    }

    void Update() {
        if (IsEquipped) {
            GetComponent<Image>().color = equippedColor;
        } else {
            GetComponent<Image>().color = new Color32(0, 0, 0, 0);
        }
    }
   
}
