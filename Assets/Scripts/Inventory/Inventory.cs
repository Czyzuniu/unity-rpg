﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;


public class Inventory : MonoBehaviour
{
    public Animator animator;
    public List<Item> items;
    public SerializableDictionaryBase<EquipmentSlot, Equipment> equipment = new SerializableDictionaryBase<EquipmentSlot, Equipment>();
    public SerializableDictionaryBase<EquipmentSlot, Transform> visualArmor = new SerializableDictionaryBase<EquipmentSlot, Transform>();
    public SerializableDictionaryBase<WeaponType, Transform> visualWeapon = new SerializableDictionaryBase<WeaponType, Transform>();

    public delegate void OnInventoryItemChanged(Equipment newItem, Equipment oldItem);
    public static OnInventoryItemChanged InventoryItemChanged;
    
    private Attributes attributes;
    private StateManager stateManager;
    private Consumable currentConsumable;

    void Awake()
    {
        foreach (string slot in System.Enum.GetNames(typeof(WeaponType))) {
            WeaponType weaponType = (WeaponType)System.Enum.Parse(typeof(WeaponType), slot);
            Transform holder = TransformUtils.GetChildGameObject(gameObject, $"{weaponType.ToString()}_PLACEHOLDER");
            visualWeapon.Add(weaponType, holder);
        }

        foreach (string slot in System.Enum.GetNames(typeof(EquipmentSlot))) {
            EquipmentSlot eqSlot = (EquipmentSlot)System.Enum.Parse(typeof(EquipmentSlot), slot);
            visualArmor.Add(eqSlot, transform);
            if (!equipment.ContainsKey(eqSlot)) {
                equipment.Add(eqSlot, null);
            } else {
                if (equipment[eqSlot] is Weapon) {
                    Weapon currentItem = (Weapon) equipment[eqSlot];
                    currentItem.Equip(visualWeapon, equipment, currentItem, transform);
                }
            }
        }
        
    }

    private void Start() {
       items = Catalog.instance.GetAll();
       animator = GetComponent<Animator>();
       attributes = GetComponent<Attributes>();
       stateManager = attributes.stateManager;
    }

    public void UseItem(Item item) {
        
    }

    public void UseItem(DrinkingConsumable item) {
        WeaponController weaponController = GetComponent<WeaponController>();
        animator.SetTrigger("Drink");  
        currentConsumable = (Consumable) item;
        GameObject visual = Instantiate(item.prefab, Vector3.zero, item.prefab.transform.rotation);
        visual.transform.SetParent(weaponController.leftHand, false);
        Destroy(visual, 3.5f);
    }

    public void ApplyConsumable(AnimationEvent e) {
        print(e.animatorClipInfo.weight);
        currentConsumable.ApplyAttributes(attributes);
    }

    public void ApplyConsumableEnd() {
        currentConsumable = null;
    }

    public void UseItem(Weapon item) {
        if (stateManager.canAttack) {
            return;
        }
        bool isEquipped = equipment[item.slot] != null;
        if (isEquipped) {
            Weapon currentlyEquipped = (Weapon) equipment[item.slot];
            if (currentlyEquipped.name == item.name) {
                item.UnEquip(visualWeapon, equipment, currentlyEquipped, transform);
            } else {
                item.UnEquip(visualWeapon, equipment, currentlyEquipped, transform);
                item.Equip(visualWeapon, equipment, item, transform);
            }  
        } else {
           item.Equip(visualWeapon, equipment, item, transform);
        }
    }
}
