﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUI : PlayerUI
{

    private Transform hpPanel;

    void Awake() {
        myInventory = GetComponent<Inventory>();
        UI = transform.Find("UI");
        hpPanel = UI.Find("HP_PANEL");
        healthBar = UI.Find("HP_PANEL/HealthBar").GetComponent<Slider>();
        healthStatusText = UI.Find("HP_PANEL/HealthBar/HPStatusText").GetComponent<TMP_Text>();
        attributes = GetComponent<Attributes>();
    }

    

    void LateUpdate()
    {
        UI.transform.LookAt(transform.position + Camera.main.transform.forward);        
    }

    void Update() {
        UpdateHealthBar();
    }

    private void OnEnable() {
        
    }

    private void OnDisable() {
        
    }
}
