﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    protected Slider healthBar;
    protected Slider manaBar;
    protected TMP_Text healthStatusText;
    protected TMP_Text manaStatusText;
    protected Transform UI;
    protected Attributes attributes;
    protected Transform inventory;
    private Transform inventoryContainer;
    public GameObject inventoryItemPrefab;
    protected Inventory myInventory;
    private PlayerInputActions inputActions;

    void Awake() {
        inputActions = new PlayerInputActions();
        myInventory = GetComponent<Inventory>();
        UI = transform.Find("UI");
        healthBar = UI.Find("HP_PANEL/HealthBar").GetComponent<Slider>();
        inventoryContainer = UI.Find("Inventory");
        inventory = UI.Find("Inventory/ITEM_PANEL");
        healthStatusText = UI.Find("HP_PANEL/HealthBar/HPStatusText").GetComponent<TMP_Text>();
        attributes = GetComponent<Attributes>();
        inputActions.PlayerControls.OpenInventory.performed += ctx => ToggleInventory();

    }

    void Start() {
        healthBar.maxValue = attributes.GetAttributeValue(Attribute.Health);
    }

    void ToggleInventory() {
        inventoryContainer.gameObject.SetActive(!inventoryContainer.gameObject.activeInHierarchy);
        RenderInventory();
    }

    void DestroyAllChildren(Transform uiElement) {
        foreach (Transform child in uiElement) {
            Destroy(child.gameObject);
        }
    }

    void UseItem() {
        GameObject clickedItem = EventSystem.current.currentSelectedGameObject;
        InventoryItem item = clickedItem.GetComponent<InventoryItem>();
        item.GetItem().UseItem(gameObject);
        UpdateInventory();
    }

    void RenderInventory() {
        if (inventory.gameObject.activeInHierarchy) {
            DestroyAllChildren(inventory);
            List<Item> myItems = myInventory.items;
            foreach (Item item in myItems) {
                GameObject inventoryItem = Instantiate(inventoryItemPrefab);
                Button button = inventoryItem.GetComponent<Button>();
                button.onClick.AddListener(UseItem);
                inventoryItem.GetComponent<ItemManager>().SetItem(item);
                inventoryItem.transform.SetParent(inventory, false);
                
                if (item is Equipment) {
                    Equipment eqItem = (Equipment) item;
                    InventoryItem invItem = inventoryItem.GetComponent<InventoryItem>();
                    invItem.IsEquipped = myInventory.equipment[eqItem.slot] != null && myInventory.equipment[eqItem.slot].name == item.name;
                }
            }
        }
    }

    void UpdateInventory() {
        InventoryItem[] inventoryItems = inventory.GetComponentsInChildren<InventoryItem>();
        foreach (InventoryItem invItem in inventoryItems) {
            if (invItem.GetItem() is Equipment) {
                Equipment item = (Equipment) invItem.GetItem();
                invItem.IsEquipped = myInventory.equipment[item.slot] != null && myInventory.equipment[item.slot].name == item.name;
            }
        }
    }


    void Update() {
        //UpdateManaBar();
        UpdateHealthBar();
    }

    public void UpdateHealthBar() {
        UpdateBar(healthBar, attributes.GetAttributeValue(Attribute.Health), healthStatusText);
    }

    public void UpdateManaBar() {
        //UpdateBar(manaBar, stats.TotalMana, stats.CurrentMana, manaStatusText);
    }

    void UpdateBar(Slider bar, float val, TMP_Text linkedText) {
        if (bar) {
            bar.value = val;
            if (linkedText) {
                linkedText.text = bar.value.ToString();
            }
        }
    }

    private void OnEnable() {
        inputActions.Enable();
    }

    private void OnDisable() {
        inputActions.Disable();
    }


    protected void OnDestroy() {
        foreach (Transform child in UI.transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

}
