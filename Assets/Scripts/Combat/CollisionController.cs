﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    private void Start() {
        GetComponent<BoxCollider>().enabled = false;
    }

    public void ToggleCollision(int enable) {
        GetComponent<BoxCollider>().enabled = enable == 1;
    }
}
