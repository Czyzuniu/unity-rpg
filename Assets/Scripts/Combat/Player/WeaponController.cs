﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {
    PlayerInputActions inputActions;
    public Transform rightHand;
    public Transform leftHand;

    protected Vector3 startingWeaponPos;
    protected Animator animator;

    protected Inventory inventory;

    [SerializeField]
    protected Weapon mainHand;
    [SerializeField]
    protected Weapon offHand;

    protected EquipmentSlot lastCombatMode;
    protected StateManager stateManager;

    void Awake() {
        inputActions = new PlayerInputActions();
        inputActions.PlayerControls.CombatModeMainWeapon.performed += ctx => ToggleCombatMode(EquipmentSlot.MAIN_HAND);
        inputActions.PlayerControls.CombatModeRangedWeapon.performed += ctx => ToggleCombatMode(EquipmentSlot.RANGED);
        this.GetDefault();
    }

    protected void GetDefault() {
        inventory = GetComponent<Inventory>();
        animator = GetComponent<Animator>();
        stateManager = GetComponent<StateManager>();
        this.GetSpecific();
    }

    protected virtual void GetSpecific() {
        leftHand = TransformUtils.GetChildGameObject(gameObject, "LeftHand");
        rightHand = TransformUtils.GetChildGameObject(gameObject, "RightHand");
    }

    public void ToggleCombatMode(EquipmentSlot mode) {
        Equipment currentInSlot = inventory.equipment[mode];
        if (!currentInSlot) return; 
        stateManager.IsInCombatStance = !stateManager.IsInCombatStance;
        if (stateManager.IsInCombatStance) {
            lastCombatMode = mode;
        } 
        TriggerCombatAnimations();
    }

    public void StartCombatMode() {
        stateManager.IsInCombatStance = true;
        TriggerCombatAnimations();
    }

    public void EndCombatMode() {
        stateManager.IsInCombatStance = false;
        TriggerCombatAnimations();
    }

    void TriggerCombatAnimations() {
        animator.SetBool("InCombatStance", stateManager.IsInCombatStance);
        if (stateManager.IsInCombatStance) {
            animator.SetTrigger("WithdrawWeapon");
        }  else {
            animator.SetTrigger("SheathWeapon");
        }
    }

    void Update() {
        mainHand = (Weapon)inventory.equipment[EquipmentSlot.MAIN_HAND];
        offHand = (Weapon)inventory.equipment[EquipmentSlot.OFF_HAND];
    }

    public Transform GetHolderByWeaponType(WeaponType weaponType) {
        return inventory.visualWeapon[weaponType];
    }

    public Transform GetWeaponObject(WeaponType weaponType) {
        return GetHolderByWeaponType(weaponType).GetChild(0);
    }

    public Transform GetHandBySlotType(EquipmentSlot slot) {
        Transform handObject = rightHand.transform;
        if (EquipmentSlot.OFF_HAND == slot) {
            handObject = leftHand;
        }

        return handObject;
    }

    public Transform GetWeaponInHandObject(EquipmentSlot slot) {
        return GetHandBySlotType(slot).GetChild(0);
    }

    public void WithdrawSheathHand(EquipmentSlot weaponSlot, int mode) {
        Weapon currentInSlot = (Weapon) inventory.equipment[weaponSlot];
        if (!currentInSlot) return;
        Transform holder = this.GetHolderByWeaponType(currentInSlot.weaponType);
        Transform handObject = GetHandBySlotType(weaponSlot);

        Transform weaponObject = null;

        if (mode == 0) {
            weaponObject = holder.GetChild(0);
            weaponObject.transform.SetParent(handObject, true);
            weaponObject.transform.position = handObject.position;
            weaponObject.transform.rotation = handObject.rotation;
        } else {
            weaponObject = handObject.transform.GetChild(0);
            weaponObject.transform.SetParent(holder, true);
            weaponObject.transform.localPosition = Vector3.zero;
            weaponObject.transform.localRotation = Quaternion.identity;
        }
    }

    public void WithdrawWeapon() {
        WithdrawSheathHand(EquipmentSlot.MAIN_HAND, 0);
        //WithdrawSheathHand(EquipmentSlot.OFF_HAND, 0);
        stateManager.CanAttack = true;
    }

    public void SheathWeapon() {
        stateManager.CanAttack = false;
        WithdrawSheathHand(EquipmentSlot.MAIN_HAND, 1);
        WithdrawSheathHand(EquipmentSlot.OFF_HAND, 1);
    }

    void OnEnable() {
        inputActions.Enable();
    }

    void OnDisable() {
        inputActions.Disable();
    }

    public void HitToggle(int enable) {
        Transform weaponObject = GetWeaponInHandObject(lastCombatMode);
        if (weaponObject) {
            CollisionController collisionController = weaponObject.GetComponent<CollisionController>();
            collisionController.ToggleCollision(enable);
        }
    }

}


