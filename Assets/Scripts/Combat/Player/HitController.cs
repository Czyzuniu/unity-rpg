﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitController : MonoBehaviour
{
    private Animator animator;
    
    
    public delegate void OnWaterEntered();
    public static OnWaterEntered WaterEntered;

    public delegate void OnSuccessBlock();
    public static OnSuccessBlock BlockSuccess;


    void Start() {
        animator = GetComponent<Animator>();
    }

    void OnWaterEnter() {
        WaterEntered?.Invoke();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.transform.IsChildOf(this.transform)) {
            return;
        }

        if (LayerMask.NameToLayer("Water") == other.gameObject.layer) {
            OnWaterEnter();
        }

        if (LayerMask.NameToLayer("Weapon") == other.gameObject.layer) {
            Attributes attributes = GetComponent<Attributes>();
            StateManager stateManager = attributes.stateManager;
            Vector3 spawnPoint = other.gameObject.transform.position;

            if (stateManager.isBlocking) {
                Catalog.instance.effect["SPARK"].Spawn(spawnPoint);
                Catalog.instance.vfx["SWORD_SWORD_CLASH"].Play(other.gameObject);
                BlockSuccess?.Invoke();
                return;
            }

            Catalog.instance.effect["BLOOD_HIT"].Spawn(spawnPoint);
            attributes.ApplyValueToAttribute(Attribute.Health, -15);
            animator.SetTrigger("Hit");
        }
        
    }


    public void Hit(int gotHit) {
        StateManager stateManager = GetComponent<StateManager>();
        stateManager.isHit = gotHit == 1;

        animator.SetBool("IsHit", stateManager.isHit);
    }
}
