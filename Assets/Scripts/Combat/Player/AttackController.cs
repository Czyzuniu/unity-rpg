﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AttackController : MonoBehaviour
{

    [SerializeField]
    protected int currentComboStep;

    protected PlayerInputActions inputActions;
    protected Animator animator;
    protected StateManager stateManager;


    void Awake()
    {
        inputActions = new PlayerInputActions();
        animator = GetComponent<Animator>();
        inputActions.PlayerControls.StandardAttack.performed += ctx => Attack();
        inputActions.PlayerControls.Block.performed += ctx => Block(true);
        inputActions.PlayerControls.Block.canceled += ctx => Block(false);
        stateManager = GetComponent<StateManager>();
    }

    public virtual void Attack() {
        if (!stateManager.IsInCombatStance || !stateManager.CanAttack) return;
        animator.SetTrigger("Attack");
        stateManager.CanAttack = false;
    }


    public virtual void Block(bool isBlocking) {
        if (!stateManager.IsInCombatStance) return;
        animator.SetBool("IsBlocking", isBlocking);
    }


    private void OnEnable() {
        if (inputActions != null) {
            inputActions.Enable();
        }
    }

    private void OnDisable() {
        if (inputActions != null) 
        inputActions.Disable();
    }

    public virtual void ComboWindowStart(int nextAttackId) {
        currentComboStep = nextAttackId;
        stateManager.CanAttack = true;
        animator.SetFloat("AttackAnimationSpeed", 0.2f);
    }

    public virtual void ComboWindowEnd() {
        stateManager.CanAttack = true;
        animator.ResetTrigger("Attack");
        currentComboStep = 0;
        animator.SetFloat("AttackAnimationSpeed", 1f);
    }

    void Update() {
        animator.SetInteger("AttackStep", currentComboStep);
    }
}
