﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AttackControllerAI : AttackController
{
    void Awake()
    {
        animator = GetComponent<Animator>();
        stateManager = GetComponent<StateManagerAI>();
    }

    public override void ComboWindowStart(int nextAttackId) {
        currentComboStep = nextAttackId;
        stateManager.CanAttack = true;
    }

    public override void ComboWindowEnd() {
        currentComboStep = 0;
    }
    
    public override void Attack() {
        if (!stateManager.IsInCombatStance || !stateManager.CanAttack) return;
        animator.SetTrigger("Attack");  
    }

    void Update() {
        animator.SetInteger("AttackStep", currentComboStep);
    }
}
