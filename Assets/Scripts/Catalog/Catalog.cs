﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;


public class Catalog : MonoBehaviour
{
    public SerializableDictionaryBase<string, Item> items = new SerializableDictionaryBase<string, Item>();
    public SerializableDictionaryBase<string, Audio> speech = new SerializableDictionaryBase<string, Audio>();
    public SerializableDictionaryBase<string, Audio> vfx = new SerializableDictionaryBase<string, Audio>();
    public SerializableDictionaryBase<string, Effect> effect = new SerializableDictionaryBase<string, Effect>();
    public static Catalog instance;

    void Awake() {
        if (instance != null) {
            Destroy(gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        Object[] loaded = Resources.LoadAll("Items", typeof(Item));
        foreach (Item t in loaded) {
            items.Add(t.name, t);
        }

        loaded = Resources.LoadAll("Audio/Speech", typeof(Audio));
        foreach (Audio t in loaded) {
            speech.Add(t.name, t);
        }

        loaded = Resources.LoadAll("Audio/VFX", typeof(Audio));
        foreach (Audio t in loaded) {
            vfx.Add(t.name, t);
        }

        loaded = Resources.LoadAll("Effect", typeof(Effect));
        foreach (Effect t in loaded) {
            effect.Add(t.name, t);
        }

    }

    public List<Item> GetAll() {
        return new List<Item>(items.Values);
    }

    public Item GetItem(string itemName) {
        if (items.ContainsKey(itemName)) {
            return items[itemName];
        }
        return null;
    }
}
