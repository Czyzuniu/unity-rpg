﻿
public enum Attribute { MovementSpeed, Health, Mana, BaseDamage }

public enum Guild {
   SKELETON, WOLF, ORC 
}

public enum ItemType {
    CONSUMABLE, WEAPON, ARMOR, SPELL, QUEST
}

public enum WeaponType {
    ONE_HAND, TWO_HAND, SHIELD, BOW, CROSSBOW
}

public enum EquipmentSlot {
   HEAD, CHEST, MAIN_HAND, OFF_HAND, RANGED
}


public enum AIBehaviourState {
    IDLE, PATROL, CHASE, ATTACK, DODGE, HIT, PARRY, FLEE, IN_PROXIMITY, IN_PROXIMITY_DECIDING, IN_PROXIMITY_TRY_ATTACK
}
