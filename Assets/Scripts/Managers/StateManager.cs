﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public bool isBlocking;
    public bool isMoving;
    public bool isInCombatStance;
    public bool isHit;
    public bool isInWater;
    public bool canAttack;
    public bool isDead;
    [SerializeField]
    private bool canMove;
    public bool isStopped;
    public bool inAir;
    [SerializeField]
    private bool isFalling;
    [SerializeField]
    private bool isJumping;

    public bool IsMoving {
        get => isMoving;
        set => isMoving = value;
    }
    public bool IsInCombatStance {
        get => isInCombatStance;
        set => isInCombatStance = value;
    }
    public bool IsStaggered {
        get => isHit;
        set => isHit = value;
    }
    public bool CanAttack {
        get => canAttack;
        set => canAttack = value;
    }
    public bool IsAttacking {
        get => isStopped;
        set => isStopped = value;
    }
    public bool InAir {
        get => inAir;
        set => inAir = value;
    }
    public bool IsFalling {
        get => isFalling;
        set => isFalling = value;
    }
    public bool IsJumping {
        get => isJumping;
        set => isJumping = value;
    }
    public bool CanMove {
        get => canMove;
        set => canMove = value;
    }
}
