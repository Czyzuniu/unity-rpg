﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public Item entity;


    public Weapon GetWeapon() {
        return (Weapon) entity;
    }

    public Item GetItem() {
        return entity;
    }

    public void SetItem(Item newItem) {
        this.entity = newItem;
    }
}
