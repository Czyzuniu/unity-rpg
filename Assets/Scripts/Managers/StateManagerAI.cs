﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManagerAI : StateManager
{
    public AIBehaviourState state;
    public bool IsHitWalkBack;
    public bool InCombatRange;
    public bool IsInChasingRange;
    public bool InAttackRange;

    private void Start() {
        state = AIBehaviourState.IDLE;
    }
}
