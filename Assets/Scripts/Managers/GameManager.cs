using UnityEngine.Audio;
using System;
using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public GameObject controllerPlayer;
    public Cinemachine.CinemachineFreeLook mainCamera;
    public List<GameObject> inCombatWith;
    public bool inCombat;

    void Awake() {
        if (instance != null) {
            Destroy(gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start() {
        AudioManager.instance.TogglePlay("MusicZone1");
    }

    public void OnCombatStart(GameObject combatObject) {
        inCombatWith.Add(combatObject);
        ToggleCombat();
    }

    private void ToggleCombat() {
        bool alreadyInCombat = this.inCombat;
        inCombat = inCombatWith.Count > 0;
        if (inCombat && !alreadyInCombat) {
            AudioManager.instance.TogglePlay("CombatMusic");
        } else if (!inCombat && alreadyInCombat) {
            AudioManager.instance.TogglePlay("MusicZone1");
        }
    }

    public void OnCombatEnd(GameObject combatObject) {
        inCombatWith.Remove(combatObject);
        ToggleCombat();
    }
}
