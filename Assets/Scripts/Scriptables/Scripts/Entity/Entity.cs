﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName= "New Entity", menuName="Entity")]
public class Entity : ScriptableObject
{
    public string entityName;
    public int level;
    public Guild guild;
}
