﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName= "New Entity", menuName="Entity/Non Playable Enity (NPC)")]
public class NonPlayableEntity : Entity
{
    public float aggroDistance;
    public float chaseSpeed;
    public float walkSpeed;
    public float combatWalkSpeed;
}
