﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;


[CreateAssetMenu(fileName = "New Equipment", menuName = "Item/Equipment")]
public class Equipment : Item {
    public EquipmentSlot slot;

    public virtual string GetSlotName() {
        return $"{slot.ToString()}_PLACEHOLDER";
    }

    public virtual void Equip(SerializableDictionaryBase<EquipmentSlot, Equipment> equipment, Equipment item) {
        equipment[item.slot] = item;
    }

    public virtual void UnEquip(SerializableDictionaryBase<EquipmentSlot, Equipment> equipment, Equipment item) {
        equipment[item.slot] = null;
    }
}
