﻿
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System.Text;
using System.Collections.Generic;

[CreateAssetMenu (fileName= "New Consumable", menuName="Item/Consumable")]
public class Consumable : Item
{
    public SerializableDictionaryBase<Attribute, float> attributes;

    public override string GetItemDetails() {
        StringBuilder sb = new StringBuilder();
        foreach(KeyValuePair<Attribute,float> attachStat in attributes)
        {
            sb.AppendLine($"{attachStat.Key.ToString()} : {attachStat.Value}");
        }
        return sb.ToString();
    }

    public override void UseItem(GameObject user) {
        Inventory inventory = user.GetComponent<Inventory>();
        inventory.UseItem(this);
    }

    public virtual void ApplyAttributes(Attributes attributes) {
        foreach(KeyValuePair<Attribute,float> attachStat in this.attributes)
        {
            attributes.ApplyValueToAttribute(attachStat.Key, attachStat.Value);
        }
    }
}