﻿
using UnityEngine;

[CreateAssetMenu (fileName= "New Game Item", menuName="Item")]
public class Item : ScriptableObject
{
    public string itemName;
    public GameObject prefab;
    public Sprite inventorySprite;

    public virtual string GetItemDetails() {
        return itemName;
    }

    public virtual void UseItem(GameObject user) {
        Inventory inventory = user.GetComponent<Inventory>();
        inventory.UseItem(this);
    }
}



