﻿
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System.Text;
using System.Collections.Generic;

[CreateAssetMenu (fileName= "New Consumable", menuName="Item/Consumable/Drink")]
public class DrinkingConsumable : Consumable
{
    public override void UseItem(GameObject user) {
        Inventory inventory = user.GetComponent<Inventory>();
        inventory.UseItem(this);
    }
}