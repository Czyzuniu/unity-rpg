﻿
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu (fileName= "New Weapon", menuName="Item/Equipment/Weapon")]
public class Weapon : Equipment
{
    public float attackSpeed;
    public float minDamage;
    public float maxDamage;
    public WeaponType weaponType;
    public AnimatorOverrideController weaponAnimator;


    public int CalculateDealtDamage() {
        return (int) Random.Range(minDamage, maxDamage);
    }

    public override string GetItemDetails() {
        return $"Attack Speed : {attackSpeed}s \n Damage : {minDamage} - {maxDamage} \n Type: {weaponType.ToString()}";
    }

    public override void UseItem(GameObject user) {
        Inventory inventory = user.GetComponent<Inventory>();
        inventory.UseItem(this);
    }

    public void Equip(SerializableDictionaryBase<WeaponType, Transform> visualWeapon, SerializableDictionaryBase<EquipmentSlot, Equipment> equipment, Weapon item, Transform target) {
        base.Equip(equipment, (Equipment) item);
        GameObject weaponObject = Instantiate(item.prefab, Vector3.zero, Quaternion.identity);
        weaponObject.transform.SetParent(visualWeapon[item.weaponType], false);
        if (weaponAnimator) {
            TransformUtils.SetAnimator(target, weaponAnimator);
        }
    }


    public void UnEquip(SerializableDictionaryBase<WeaponType, Transform> visualWeapon, SerializableDictionaryBase<EquipmentSlot, Equipment> equipment, Weapon item, Transform target) {
        base.UnEquip(equipment, item);
        Transform holder = visualWeapon[item.weaponType];
        if (holder) {
            GameObject currentObject = holder.GetChild(0).gameObject;
            if (currentObject) {
                Destroy(currentObject);
            }
        }
        Attributes targetAttributes = target.GetComponent<Attributes>(); 
        if (targetAttributes) {
            TransformUtils.SetAnimator(target, targetAttributes.defaultAnimatorController);
        }
    }
    
}