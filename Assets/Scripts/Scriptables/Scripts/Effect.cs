﻿
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu (fileName= "New Effect", menuName="Effect")]
public class Effect: ScriptableObject
{
    public GameObject particleEffect;


    public void Spawn(Vector3 position) {
        Destroy(Instantiate(particleEffect, position, Quaternion.identity), 0.5f);
    }
}