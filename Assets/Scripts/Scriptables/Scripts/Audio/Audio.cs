﻿
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu (fileName= "New Audio", menuName="Audio")]
public class Audio: ScriptableObject
{
    public AudioClip clip;
    public float spatialBlend;
    public AudioRolloffMode rolloffMode;

    public void Play(GameObject from) {
        AudioSource audioSource = from.AddComponent<AudioSource>();   
        audioSource.spatialBlend = spatialBlend;
        audioSource.rolloffMode = rolloffMode;
        audioSource.clip = clip;
        audioSource.Play();
    }
}