%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: InteractionMask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Hand IK.L
    m_Weight: 1
  - m_Path: Armature/Hand IK.L/Hand IK.L_end
    m_Weight: 1
  - m_Path: Armature/Hand IK.L.001
    m_Weight: 1
  - m_Path: Armature/Hand IK.L.001/Hand IK.L.001_end
    m_Weight: 1
  - m_Path: Armature/Hips
    m_Weight: 1
  - m_Path: Armature/Hips/Core
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Neck
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Finger 1
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Finger 1/Finger 1
      middle
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Finger 1/Finger 1
      middle/Finger 1 middle.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Finger 1/Finger 1
      middle/Finger 1 middle.001/Finger 1 middle.001_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger/Middle
      finger tip
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger/Middle
      finger tip/Middle finger tip.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger/Middle
      finger tip/Middle finger tip.001/Middle finger tip.001_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger.001/Middle
      finger.002
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger.001/Middle
      finger.002/Middle finger.003
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Middle finger.001/Middle
      finger.002/Middle finger.003/Middle finger.003_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Pinky
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Pinky/Pinky tip
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Pinky/Pinky tip/Pinky
      tip.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Pinky/Pinky tip/Pinky
      tip.001/Pinky tip.001_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Thumb
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Thumb/Thumb tip
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Thumb/Thumb tip/Thumb
      tip.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder/Arm/Forearm/Wrist/Thumb/Thumb tip/Thumb
      tip.001/Thumb tip.001_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Finger
      1.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Finger
      1.001/Finger 1 middle.002
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Finger
      1.001/Finger 1 middle.002/Finger 1 middle.003
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Finger
      1.001/Finger 1 middle.002/Finger 1 middle.003/Finger 1 middle.003_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.004
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.004/Middle finger tip.002
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.004/Middle finger tip.002/Middle finger tip.003
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.004/Middle finger tip.002/Middle finger tip.003/Middle finger tip.003_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.005
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.005/Middle finger.006
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.005/Middle finger.006/Middle finger.007
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Middle
      finger.005/Middle finger.006/Middle finger.007/Middle finger.007_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Pinky.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Pinky.001/Pinky
      tip.002
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Pinky.001/Pinky
      tip.002/Pinky tip.003
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Pinky.001/Pinky
      tip.002/Pinky tip.003/Pinky tip.003_end
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Thumb.001
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Thumb.001/Thumb
      tip.002
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Thumb.001/Thumb
      tip.002/Thumb tip.003
    m_Weight: 1
  - m_Path: Armature/Hips/Core/Chest/Shoulder.001/Arm.001/Forearm.001/Wrist.001/Thumb.001/Thumb
      tip.002/Thumb tip.003/Thumb tip.003_end
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh/Calf
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh/Calf/foot
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh/Calf/foot/Toes
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh/Calf/foot/Toes/Toes_end
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh.001
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh.001/Calf.001
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh.001/Calf.001/foot.001
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh.001/Calf.001/foot.001/Toes.001
    m_Weight: 1
  - m_Path: Armature/Hips/Thigh.001/Calf.001/foot.001/Toes.001/Toes.001_end
    m_Weight: 1
  - m_Path: Armature/Leg IK.L
    m_Weight: 1
  - m_Path: Armature/Leg IK.L/Leg IK.L_end
    m_Weight: 1
  - m_Path: Armature/Leg IK.L.001
    m_Weight: 1
  - m_Path: Armature/Leg IK.L.001/Leg IK.L.001_end
    m_Weight: 1
  - m_Path: Armature/Pole Target.L
    m_Weight: 1
  - m_Path: Armature/Pole Target.L/Pole Target.L_end
    m_Weight: 1
  - m_Path: Armature/Pole Target.L.001
    m_Weight: 1
  - m_Path: Armature/Pole Target.L.001/Pole Target.L.001_end
    m_Weight: 1
  - m_Path: Body
    m_Weight: 1
